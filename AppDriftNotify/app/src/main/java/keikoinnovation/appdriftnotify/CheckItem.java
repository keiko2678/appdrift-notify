package keikoinnovation.appdriftnotify;


import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

class CheckItem {
    public String pictureColour;
    public String date;
    public String status;
    public String desctiption;
    public Boolean pageStatus;
    public Boolean textStatus;
    public float timeToDownload;
    public float reward;

    CheckItem(String date, Boolean pageStatus, Boolean textStatus, float timeToDownload, float reward, String status){
        this.date = date;
        this.pageStatus = pageStatus;
        this.textStatus = textStatus;
        this.timeToDownload = timeToDownload;
        this.reward = reward;
        this.status = status;

        if(!textStatus){
            this.status = "ALARM !";
            desctiption = "Database down";
            pictureColour = "red";
        }
        else if (!pageStatus){
            this.status = "ALARM !";
            desctiption = "Webpage is down";
            pictureColour = "red";
        }
        else if (timeToDownload > 15 ){
            this.status = "Slow response";
            desctiption = "Possible webserver down";
            pictureColour = "yellow";
        }
        else if (timeToDownload > 8 ) {
            this.status = "OK !";
            desctiption = "Possible high traffic";
            pictureColour = "green";
        } else if (timeToDownload > 4 ){
            this.status = "OK !!";
            desctiption = "Good performance";
            pictureColour = "green";
        } else {
            this.status = "OK !!!";
            desctiption = "Super performance";
            pictureColour = "green";
        }
    }
}