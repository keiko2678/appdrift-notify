package keikoinnovation.appdriftnotify;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A custom ArrayAdapter class for setting item values in TextViews
 *
 * http://www.vogella.com/tutorials/AndroidListView/article.html
 * http://developer.android.com/training/improving-layouts/smooth-scrolling.html#ViewHolder
 * http://stackoverflow.com/questions/13164054/viewholder-good-practice
 */
class CheckedListArrayAdapter extends ArrayAdapter<CheckItem> {

    private final Context context;
    private ArrayList<CheckItem> values = null;
    private ImageView red_image_in_list;
    private ImageView green_image_in_list;
    private ImageView yellow_image_in_list;
    private TextView date_in_list;
    private TextView status_in_list;
    private TextView description;
    private TextView timeToDownload;


    public CheckedListArrayAdapter(Context context, int layoutResourceId, ArrayList<CheckItem> values) {
        super(context, layoutResourceId, values);
        this.context = context;
        this.values = values;


    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemInListView = convertView;

        if(itemInListView == null){
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemInListView = inflater.inflate(R.layout.array_adapter, parent, false);

            red_image_in_list = (ImageView) itemInListView.findViewById(R.id.red_image_in_list);
            green_image_in_list = (ImageView) itemInListView.findViewById(R.id.green_image_in_list);
            yellow_image_in_list = (ImageView) itemInListView.findViewById(R.id.yellow_image_in_list);
            date_in_list = (TextView) itemInListView.findViewById(R.id.date_in_list);
            status_in_list = (TextView) itemInListView.findViewById(R.id.status_in_list);
            description = (TextView) itemInListView.findViewById(R.id.desctiption_in_List);
            timeToDownload = (TextView) itemInListView.findViewById(R.id.timeToDownload);



            date_in_list.setText(values.get(position).date);
            status_in_list.setText(values.get(position).status);
            description.setText( values.get(position).desctiption);
            timeToDownload.setText("TTDL: " + String.format("%.3g%n",values.get(position).timeToDownload));

            if (values.get(position).pictureColour != null){
                if(values.get(position).pictureColour.equals("red"))
                    red_image_in_list.setVisibility(View.VISIBLE);
                else if(values.get(position).pictureColour.equals("yellow"))
                    yellow_image_in_list.setVisibility(View.VISIBLE);
                else if (values.get(position).pictureColour.equals("green"))
                    green_image_in_list.setVisibility(View.VISIBLE);
            }
        }
        return itemInListView;

    }
}

