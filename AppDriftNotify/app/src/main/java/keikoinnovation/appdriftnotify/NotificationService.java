package keikoinnovation.appdriftnotify;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * An {@link android.app.IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 *
 * Notification service that gets the newest article, video and radio show id's
 * It gets the a new list for each media and
 * If the id's ar not equal a notification i sent to the status bar notifying the user
 * that there is a new article, video or radio show
 *
 * Notification:
 * http://www.vogella.com/tutorials/AndroidNotifications/article.html#pendingintent
 * http://developer.android.com/guide/topics/ui/notifiers/notifications.html
 * http://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html
 *
 * IntentService:
 * http://it-ride.blogspot.no/2010/10/android-implementing-notification.html
 * http://code.tutsplus.com/tutorials/android-fundamentals-intentservice-basics--mobile-6183
 *
 */
public class NotificationService extends IntentService {

    public String groupUrl = "http://128.39.121.4/purser/gruppe10.txt";

    public NotificationService() {
        super("NotificationService");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Sets data from intent and calls get data functions for each media
     * @param intent intent with article id, video id and radio show id
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        new GetData().execute(groupUrl);

    }

    public class GetData extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... url) {
            String rawFeed = null;
            URL tmpUrl = null;
            try {
                tmpUrl = new URL(url[0]);
            } catch (MalformedURLException e) {
                Log.d("Warning: ", e.getMessage());
            }
            try {
                URLConnection urlCon;
                urlCon = tmpUrl.openConnection();
                InputStream is = urlCon.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);

                ByteArrayBuffer baf = new ByteArrayBuffer(262144);      // 256 KB
                int current;
                while ((current = bis.read()) != -1) {
                    baf.append((byte) current);
                }
                rawFeed = new String(baf.toByteArray(), "UTF8");

            } catch (UnsupportedEncodingException e) {
                Log.d("Warning: ", e.getMessage());
            } catch (ClientProtocolException e) {
                Log.d("Warning: ", e.getMessage());
            } catch (IOException e) {
                Log.d("Warning: ", e.getMessage());
            }
            return rawFeed;
        }


        @Override
        protected void onPostExecute(String data) {
            if (data != null) {
                String inputLine  = null;
                try {
                    BufferedReader br = new BufferedReader( new StringReader(data));

                    while ((inputLine = br.readLine()) != null) {
                            /*
                             * Regular webcheck with payment if up
                             */
                        if (inputLine.startsWith("webcheck -> Date:")) {
                            String checkDate = inputLine.split(": ")[1];
                            inputLine = br.readLine();
                            Boolean pageUp = false;
                            Boolean textUp = false;
                            float timeToDownload = 0;
                            String status = "";

                            if (inputLine.equals("Website down")) {
                                status = " ALARM ! ";
                                sendNotification("Warning",status, inputLine, 1);
                            } else {
                                pageUp = (inputLine.split(": ")[1]).equals("Yes");
                                textUp = (br.readLine().split(": ")[1]).equals("Yes");
                                timeToDownload = Float.parseFloat((br.readLine().split(": ")[1]));

                                if (!pageUp || !textUp) {
                                    status = " ALARM ! ";
                                    sendNotification("Warning",status, "WebsiteDown", 1);
                                } else {
                                    if (timeToDownload > 60) {
                                        status ="Possible high traffic";
                                        sendNotification("Info", "Extremely slow pages", status, 2);
                                    }
                                    else if (timeToDownload > 10){
                                        status ="Possible high traffic";
                                        sendNotification("Info", "Slow pages", status, 3);
                                    }
                                    status = " OK ! ";
                                }
                            }
                            br.close();
                        }
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }


    /**
     * Creates a notification for a type of media
     * @param actionType states the action id for which tab to open when user clicks on notification
     * @param title notification title
     * @param text notification text
     * @param notificationType the unique id for each media
     */
    void sendNotification(String actionType, String title, String text, int notificationType){
        Intent intent = new Intent(this, MyActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Notification notify  = new Notification.Builder(this)
                .setContentTitle(title)
                .setContentText(text)
                .setSmallIcon(getColour(notificationType))
                .setContentIntent(pIntent)
                .setContentIntent(pIntent)
                .setLights(3, 200, 50)
                .setAutoCancel(true).build();

        if(notificationType >= 2) {
            notify.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notify.vibrate = new long[]{0, 100, 200, 300};
        }
        NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(0, notify);

        Log.d("Notification: ", actionType + " - " + title + " - " + text);

    }

    int getColour(int code){
        if (code == 1){
            return R.drawable.red;
        } else if (code == 2){
            return R.drawable.yellow;
        }else
            return R.drawable.green;

    }
}

