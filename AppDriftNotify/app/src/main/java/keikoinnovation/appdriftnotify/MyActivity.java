package keikoinnovation.appdriftnotify;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;


public class MyActivity extends Activity {
    public ArrayList<CheckItem> checkList = new ArrayList<CheckItem>();
    private String rawFeed = "NoText";
    private boolean forceUpdate = false;
    public String groupUrl = "http://128.39.121.4/purser/gruppe10.txt";

    public static int NOINLIST = 3;
    private TextView textViewUpTime;
    private TextView textViewGroup;
    private TextView textFileUpdateDate;
    private TextView textViewStatus;
    private TextView textViewBalance;
    private TextView textViewEarnings;
    private TextView textViewAverageTTDL;
    private ListView ChekedList;
    private Button update;
    private Button stats;
    private ArrayAdapter arrayAdapter;

    Intent serviceIntent = null;
    PendingIntent pi = null;
    Calendar cal = Calendar.getInstance();
    AlarmManager alarm = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        textViewUpTime = (TextView) findViewById(R.id.textViewUpTime);
        textViewGroup = (TextView) findViewById(R.id.textViewGroup);
        textFileUpdateDate = (TextView) findViewById(R.id.textViewDateFileUpdate);
        textViewStatus = (TextView) findViewById(R.id.textViewStatus);
        textViewBalance = (TextView) findViewById(R.id.textViewBalance);
        textViewEarnings = (TextView) findViewById(R.id.textViewEarnings);
        textViewAverageTTDL = (TextView) findViewById(R.id.textViewAverageTTDL);
        update = (Button) findViewById(R.id.update_button);
        stats = (Button) findViewById(R.id.stats_button);

        ChekedList = (ListView) findViewById(R.id.listViewChecked);
        arrayAdapter = new CheckedListArrayAdapter(this, R.id.listViewChecked, checkList);
        arrayAdapter.setNotifyOnChange(true);
        ChekedList.setAdapter(arrayAdapter);

        new GetData().execute(groupUrl);
        update.setOnClickListener(Update);
        stats.setOnClickListener(Stats);

        alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        serviceIntent = new Intent(this, NotificationService.class);
        pi = PendingIntent.getService(this, 0, serviceIntent, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        alarm.cancel(pi);
    }



    @Override
    protected void onPause() {
        super.onPause();
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 10*60*1000, pi);
    }

    private View.OnClickListener Stats = new View.OnClickListener()  {
        @Override
        public void onClick(View view){
            String url = "http://128.39.121.4/purser/result.php?file=scoreboard.txt";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
    };

    private View.OnClickListener Update = new View.OnClickListener()  {
        @Override
        public void onClick(View view){
            forceUpdate = true;
            new GetData().execute(groupUrl);
        }
    };

    public class GetData extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
               super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... url) {
            if (checkList.isEmpty() || forceUpdate) {
                URL tmpUrl = null;
                try {
                    tmpUrl = new URL(url[0]);
                } catch (MalformedURLException e) {
                    Log.d("Warning: ", e.getMessage());
                }
                try {
                    URLConnection urlCon;
                    urlCon = tmpUrl.openConnection();
                    InputStream is = urlCon.getInputStream();
                    BufferedInputStream bis = new BufferedInputStream(is);

                    ByteArrayBuffer baf = new ByteArrayBuffer(262144);      // 256 KB
                    int current;
                    while ((current = bis.read()) != -1) {
                        baf.append((byte) current);
                    }
                    rawFeed = new String(baf.toByteArray(), "UTF8");

                } catch (UnsupportedEncodingException e) {
                    Log.d("Warning: ", e.getMessage());
                } catch (ClientProtocolException e) {
                    Log.d("Warning: ", e.getMessage());
                } catch (IOException e) {
                    Log.d("Warning: ", e.getMessage());
                }
            }
            return rawFeed;
        }


        @Override
        protected void onPostExecute(String data) {
            if (checkList.isEmpty() || forceUpdate) {

                float averagettdl = 0;
                int ttdlcount = 0;
                float reward = 0;

                checkList.clear();
                arrayAdapter.notifyDataSetChanged();

                String inputLine  = null;
                try {
                    BufferedReader br = new BufferedReader( new StringReader(data));

                    while ((inputLine = br.readLine()) != null && br.ready()) {
                            /*
                             * Regular webcheck with payment if up
                             */
                        if (inputLine.startsWith("webcheck -> Date:")) {
                            String checkDate = inputLine.split(": ")[1];
                            inputLine = br.readLine();
                            Boolean pageUp = false;
                            Boolean textUp = false;
                            float timeToDownload = 0;
                            String status = "";

                            if (inputLine.equals("Website down")) {
                                status = " ALARM ! ";
                            } else {
                                pageUp = (inputLine.split(": ")[1]).equals("Yes");
                                textUp = (br.readLine().split(": ")[1]).equals("Yes");
                                timeToDownload = Float.parseFloat((br.readLine().split(": ")[1]));


                                ttdlcount++;

                                if (!pageUp || !textUp) {
                                    status = " ALARM ! ";
                                } else {
                                    status = " OK ! ";
                                }
                            }

                            if(ttdlcount <= NOINLIST) {
                                reward += Float.parseFloat((br.readLine().split(" ")[1]));
                                averagettdl += timeToDownload;
                                checkList.add(new CheckItem(checkDate, pageUp, textUp, timeToDownload, reward, status));
                                arrayAdapter.notifyDataSetChanged();
                            } else
                                br.close();
                        }
                            /*
                             * Regular webuse
                             */
                        else if (inputLine.startsWith("webuse -> Date:")) {
                        }

                            /*
                             * Checks
                             */
                        else if (inputLine.startsWith("continuous -> Date:")) {
                        }
                            /*
                             * TROUBLE
                             */
                        else if (inputLine.startsWith("leeshore -> Date:")) {
                        }

                            /*
                             * Only date, means initial data message
                             */
                        else if (inputLine.contains("Date")) {
                            String timeUp = null;
                            String timeMaintenance = null;
                            String timeDown = null;
                            String lastUpdated = null;
                            String group = null;
                            String balance = null;

                            lastUpdated = inputLine.split(": ")[1];
                            br.readLine();
                            br.readLine();
                            group = br.readLine().split(": ")[1];
                            balance = br.readLine().split(": ")[1];
                            br.readLine();
                            timeUp = br.readLine().split(": ")[1];
                            timeMaintenance = br.readLine().split(": ")[1];
                            timeDown = br.readLine().split(": ")[1];

                            float upTime = (Float.parseFloat(timeUp) / (Float.parseFloat(timeMaintenance) + Float.parseFloat(timeDown)));

                            textFileUpdateDate.setText(lastUpdated);
                            textViewGroup.setText(group);
                            textViewBalance.setText("Balance: " + String.format("%.2f", Float.parseFloat(balance)));
                            textViewUpTime.setText("UpTime: " + String.format("%.2f",(upTime*100)));
                        }
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                if(!checkList.isEmpty()) {
                    textViewStatus.setText(checkList.get(0).status);
                    Toast.makeText(getApplicationContext(), "Updated", Toast.LENGTH_SHORT).show();
                }
                textViewAverageTTDL.setText("Average TTDL: " + String.format("%.2f",averagettdl/NOINLIST));
                textViewEarnings.setText("Average earnings: " + String.format("%.3f",reward/NOINLIST));
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
